/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.examen02;

/**
 *
 * @author Alexis
 */
public class Examen02 {

    public static void main(String[] args) {
        examen02c Examen02 = new examen02c();
        Examen02.setCodigo(147);
        Examen02.setCantidad(2.2f);
        Examen02.setPlitro(24.50f);
        Examen02.setTipo(1);
        
        System.out.println("La cantidad es : "+ Examen02.calcularCantidad2());
        System.out.println("Los litros son : "+ Examen02.calcularPlitro2());
        System.out.println("El total es: "+ Examen02.calcularTotal());
        
    }
}
