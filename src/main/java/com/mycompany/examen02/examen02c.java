/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.examen02;

/**
 *
 * @author Alexis
 */
public class examen02c {
    private int codigo;
    private float cantidad;
    private int tipo;
    private float plitro;
    private float cantidad2;
    private float total;
    public examen02c(){
        this.codigo=0;
        this.cantidad=0.0f;
        this.tipo=0;
        this.plitro=0.0f;
        this.cantidad2=0.0f;
        this.total=0.0f;
    }

    public examen02c(int codigo, float cantidad, int tipo, float plitro, float cantidad2, float total) {
        this.codigo = codigo;
        this.cantidad = cantidad;
        this.tipo = tipo;
        this.plitro = plitro;
        this.cantidad2 = cantidad2;
        this.total = total;
    }
    public examen02c (examen02c otro){
        this.codigo = otro.codigo;
        this.cantidad = otro.cantidad;
        this.tipo = otro.tipo;
        this.plitro = otro.plitro;
        this.cantidad2 = otro.cantidad2;
        this.total = otro.total;        
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public float getPlitro() {
        return plitro;
    }

    public void setPlitro(float plitro) {
        this.plitro = plitro;
    }

    public float getCantidad2() {
        return cantidad2;
    }

    public void setCantidad2(float cantidad2) {
        this.cantidad2 = cantidad2;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
   
    public float calcularCantidad2(){
        float cantidad2=this.cantidad;
        return cantidad2;
    }
    public float calcularPlitro2(){
        float plitro=this.plitro;
        return plitro;
    }
     public float calcularTotal() {
        float subtotal = 0.0f;
        if (tipo == 1){this.plitro = 24.50f;
        total=this.cantidad*this.plitro;}
        
        if (tipo == 2){this.plitro = 20.50f;
        total=this.cantidad*this.plitro;}
        
        return total;                  }
    
}
